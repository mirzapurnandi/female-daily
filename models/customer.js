const list = (con, callback) => {
    con.query('select * from customers', callback)
}

const listGroup = (con, callback) => {
    con.query('select firstname, lastname, email from customers group by email', callback)
}

module.exports = {
    list,
    listGroup
};
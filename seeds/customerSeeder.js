
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('customers').del()
    .then(function () {
      // Inserts seed entries
      return knex('customers').insert([
        {
          firstname: "Tommy",
          lastname: "Bejo",
          email: "tommy@mail.com",
          item: "Barang1",
          qty: 2,
          total: 100000
        },
        {
          firstname: "Joko",
          lastname: "Widodo",
          email: "joko@mail.com",
          item: "Barang2",
          qty: 1,
          total: 50000
        },
        {
          firstname: "Jusuf",
          lastname: "Kalla",
          email: "jusuf@mail.com",
          item: "Barang3",
          qty: 3,
          total: 150000
        },
        {
          firstname: "Tommy",
          lastname: "Bejo",
          email: "tommy@mail.com",
          item: "Barang2",
          qty: 2,
          total: 100000
        },
        {
          firstname: "Joko",
          lastname: "Widodo",
          email: "joko@mail.com",
          item: "Barang1",
          qty: 2,
          total: 100000
        },
        {
          firstname: "Jusuf",
          lastname: "Kalla",
          email: "jusuf@mail.com",
          item: "Barang2",
          qty: 2,
          total: 100000
        },
        
        {
          firstname: "Joko",
          lastname: "Widodo",
          email: "joko@mail.com",
          item: "Barang4",
          qty: 3,
          total: 130000
        },
        {
          firstname: "Jusuf",
          lastname: "Kalla",
          email: "jusuf@mail.com",
          item: "Barang5",
          qty: 3,
          total: 120000
        },
        {
          firstname: "Robert",
          lastname: "Garcia",
          email: "robert@mail.com",
          item: "Barang6",
          qty: 1,
          total: 50000
        },

        {
          firstname: "Robert",
          lastname: "Garcia",
          email: "robert@mail.com",
          item: "Barang10",
          qty: 3,
          total: 150000
        },
      ]);
    });
};

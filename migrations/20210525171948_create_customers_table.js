
exports.up = function(knex) {
    return knex.schema
        .createTableIfNotExists('customers', function(table) {
            table.increments();
            table.string('firstname', 255).nullable();
            table.string('lastname', 255).nullable();
            table.string('email', 200).nullable();
            table.string('item', 100).nullable();
            table.integer('qty').nullable();
            table.decimal('total').nullable();
            table.timestamps();
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('customers');
};

// Update with your config settings.

module.exports = {

  development: {
    client: 'mysql2',
    connection: {
      host : process.env.DB_HOST || 'localhost',
      user : process.env.DB_USER || 'root',
      password : process.env.DB_PASSWORD || '',
      database : process.env.DB_DATABASE || 'female_daily'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  staging: {
    client: 'mysql2',
    connection: {
      database: 'female_daily',
      user:     'root',
      password: ''
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};

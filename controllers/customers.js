const Customer = require('../models/customer');

const index = (req, res) => {
    Customer.list(req.con, (err, results) => {
        res.render('customer/index', {
            result: results
        });
    });
};

const pivot = (req, res) => {
    Customer.listGroup(req.con, (err, results) => {
        res.render('customer/pivot', {
            result: results
        });
    });
}

module.exports = {
    index,
    pivot
}
var express = require('express');
var router = express.Router();

const customers = require('../controllers/customers')

router.get('/', customers.index)
router.get('/pivot', customers.pivot)

module.exports = router;
